//
//  StreamkastPostMadelView.swift
//  StreamkastInterview
//
//  Created by Rahul Saini on 04/04/20.
//  Copyright © 2020 Streamkast. All rights reserved.
//

import SwiftUI

struct StreamkastPostMadelView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct StreamkastPostMadelView_Previews: PreviewProvider {
    static var previews: some View {
        StreamkastPostMadelView()
    }
}
