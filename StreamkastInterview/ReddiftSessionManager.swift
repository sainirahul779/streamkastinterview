//
//  ReddiftSessionManager.swift
//  StreamkastInterview
//
//  Created by Rahul Saini on 03/04/20.
//  Copyright © 2020 Streamkast. All rights reserved.
//

import Foundation
import reddift
import SwiftUI

class ReddiftSessionManager {
    var session: Session?
    static let shared = ReddiftSessionManager()
    static var accessToken = ""
    var postList = [RedditPostModel]()
    var links = [Link]()
    var paginator: Paginator!
    private init() {
        
    }
    
    //Intialize new session onn access call back from reddiit web page
    func intializeSession(with result: Result<OAuth2Token>) {
        print("===========")
        switch result {
        case .failure(let error):
            self.session = Session()
        case .success(let token):
            print("REDDIIFT Reciver Session Stabliszed==>>\(token) ====== \(token.name)")
            DispatchQueue.main.async {
                do {
                    try OAuth2TokenRepository.save(token: token, of: token.name)
                    self.reloadSession()
                } catch {
                    
                }
            }
        }
        
        self.paginator = Paginator(after: "", before: "", modhash: "")
        
    }
    
    
    
    //Reload session
    func reloadSession() {
        if let accesstoken = UserDefaults.standard.object(forKey: "streamkasttoken") as? String, !accesstoken.isEmpty { //reload from userdefault
            
            if let tokenDate =  UserDefaults.standard.object(forKey: "streamkastSessionTime") as? Date {
                
                let intervalSinceToken = Date().timeIntervalSince(tokenDate)
                if intervalSinceToken > 3500 {
                    self.refreshSession()
                    return
                }
            }
            do {
                let token = try OAuth2TokenRepository.token(of: "streamkasttoken")
                
                self.session = Session(token: token)
                self.paginator = Paginator(after: "", before: "", modhash: "")
                self.getList()
            } catch { print(error) }
        } else {
            self.refreshSession() //get new accesstoken
        }
    }
    
    //get new accesstoken
    func refreshSession() {
        let udid = UIDevice.current.identifierForVendor?.uuidString ?? ""
        let urlStr = "https://www.reddit.com/api/v1/authorize?client_id=diVkKEOudfa-Eg&response_type=token&state=\(udid)&redirect_uri=StreamkastInterview://response&scope=identity+read&approval_prompt=auto&duration=temporary"
        print(urlStr)
        if let url = URL(string: urlStr) {
            UIApplication.shared.open(url, options: [:]) { (status) in
                print(status)
            }
        }
    }
    
    
    
    
    
    func getList() {
        do {
            try self.session?.getList(self.paginator, subreddit: nil, sort: .top, timeFilterWithin: .all, completion: {
                (result) in
                
                switch result {
                case .failure(let error):
                    print(error)
                case .success(let listing):
                    print(listing)
                    self.paginator = listing.paginator
                    if let links = listing.children as? [Link] {
                        
                        for item in links {
                            let post = RedditPostModel(link: item)
                            self.postList.append(post)
                            self.links.append(item)
                            print(item)
                        }
                        self.openLisitingView()
                    }
                    
                    
                    
                }
            })
        } catch let error {
            print("Listinng Error========>\(error)")
        }
        
    }
    
    
    func openLisitingView() {
        let postListView = RedditPostListView()
        
        DispatchQueue.main.async {
            if let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene, let sceneDelegate = windowScene.delegate as? SceneDelegate {
                sceneDelegate.window?.rootViewController = UIHostingController(rootView: postListView)
                sceneDelegate.window?.makeKeyAndVisible()
                
            }
        }
        
    }
    
    
    func getComments(post: RedditPostModel) {
        var link: Link?
        var postComments = [Comment]()
        for i in 0..<self.postList.count where self.postList[i].id == post.id {
            link = self.links[i]
        }
        if let linkObj = link {
            do {
                try self.session?.getArticles(linkObj, sort: CommentSort.top, completion: { (result) in
//                    print(result)
                    
                    switch result {
                    case .failure(let error):
//
                        print("Comment Fetch Error=====>")
                    case .success(let tuple):
                        
                        for children in tuple.1.children {
                            if let comment = children as? Comment {
                                postComments.append(comment)
                            }
                        }
                        print(postComments.count)
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "CommentsGetForPost"), object: nil, userInfo: ["post": post,"comments": postComments])

                    }

                    
                    
                })
            } catch let error {
                print(error)
            }
        }
        
    }
}




