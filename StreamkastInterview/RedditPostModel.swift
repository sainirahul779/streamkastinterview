//
//  RedditPostModel.swift
//  StreamkastInterview
//
//  Created by Rahul Saini on 04/04/20.
//  Copyright © 2020 Streamkast. All rights reserved.
//

import Foundation
import reddift


struct RedditPostModel: Identifiable, Hashable {
    var id: UUID
    
    var postId: String = ""
    var title = ""
    var name = ""
    var subreddit = ""
    var author = ""
    var thumbnnil = ""
    var numComments = 0
    var ups = 0
  
    var thumbURL: URL? {
        if let url = URL(string: self.thumbnnil) {
            return url
        }
        return URL(string: "https://b.thumbs.redditmedia.com/2Y7_nTaEUYJRyYGICYoXfKCEazVQRHib4XJQ1CNQC8Y.jpg")
    }
    init(link: Link) {
        
        print("modelCreated=====")
        print(link.thumbnail)
        self.id = UUID()
        postId = link.id
        title = link.title
        name = link.name
        subreddit = link.subreddit
        author = link.author
        thumbnnil = link.thumbnail
        numComments = link.numComments
        ups = link.ups
        
    }
    
    init() {
        self.id = UUID()
        postId = "dsvcvfds"
        title = "dsfdsc"
        name = "dfdssv"
        subreddit = "ghjbkn"
        author = "hgvjbkn"
    }
    
    
}



extension Int {
    func getThosendDevidedVale() -> String {
        if self > 1000 {
            let value = 10 * (Double(self) / 1000)
            let inThousend: Double = round(value) / 10
            return "\(inThousend)k"
        }
        return "\(self)"
    }
}
