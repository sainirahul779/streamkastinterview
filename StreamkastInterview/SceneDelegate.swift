//
//  SceneDelegate.swift
//  StreamkastInterview
//
//  Created by Rahul Saini on 03/04/20.
//  Copyright © 2020 Streamkast. All rights reserved.
//

import UIKit
import reddift

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var session: Session?

    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let _ = (scene as? UIWindowScene) else { return }
        
        ReddiftSessionManager.shared.reloadSession()
                   
    }

    @available(iOS 13.0, *)
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    @available(iOS 13.0, *)
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    @available(iOS 13.0, *)
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    @available(iOS 13.0, *)
    func sceneWillEnterForeground(_ scene: UIScene) {
    }

    @available(iOS 13.0, *)
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.

        // Save changes in the application's managed object context when the application transitions to the background.
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }

    
    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
       
        if let url = URLContexts.first?.url {
            let urlparams = url.absoluteString.components(separatedBy: "#")
            let token = OAuth2Token.tokenWithJSONStreamkast(urlparams[1])
            ReddiftSessionManager.shared.intializeSession(with: token)
        }
    }
}








extension OAuth2Token {
    static func tokenWithJSONStreamkast(_ jsonString: String) -> Result<OAuth2Token> {
        print("########")
        let params = jsonString.components(separatedBy: "&")
        var parameter: [String: Any] = [:]
        for param in params {
            let components = param.components(separatedBy: "=")
            if components[0] == "access_token" {
                UserDefaults.standard.set(components[1], forKey: "streamkasttoken")
                
                parameter["access_token"] = components[1]
            } else if components[0] == "token_type" {
                parameter["token_type"] = components[1]
            } else if components[0] == "state" {
                parameter["state"] = components[1]
            } else if components[0] == "expires_in" {
                parameter["expires_in"] = components[1]
            } else if components[0] == "scope" {
                parameter["scope"] = components[1]
            }
            parameter["refresh_token"] = ""
            parameter["name"] = "streamkasttoken"
            
        }
        UserDefaults.standard.set(Date(), forKey: "streamkastSessionTime")
        return Result(value: OAuth2Token(parameter as JSONDictionary))
       }
}
