//
//  RedditPostListView.swift
//  StreamkastInterview
//
//  Created by Rahul Saini on 04/04/20.
//  Copyright © 2020 Streamkast. All rights reserved.
//

import SwiftUI
import reddift
import URLImage

struct RedditPostListView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let variable = ReddiftSessionManager.shared
    @State var isPresented = false
    var body: some View {
        let posts = ReddiftSessionManager.shared.postList
        
        return NavigationView {
            List(posts) { post in
                
                VStack(alignment: HorizontalAlignment.leading, spacing: CGFloat(10.0)) {
                    
                    RedditPostListHeaderView(post: post)
                    
                    RedditPostListPostDescrriptionView(post: post)
                    
                    HStack  {
                        HStack(spacing: 15) {
                            RedditPostShareView()
                            Image(systemName: "star.circle.fill")
                                .foregroundColor(Color.gray)
                            
                            Button(action: {
                                self.isPresented.toggle()
                            }) {
                                RedditPostListCommentView(post: post)
                                
                            }.sheet(isPresented: self.$isPresented) {
                                    RedditPostCommentsView(post: post)
                            }
                        }
                        Spacer()
                        RedditPostLisLikeDisLikeViiew(post: post)
                    }
                }
                .padding(.top, 15)
                .padding(.bottom, 15)
                
            }
            .navigationBarTitle("Streamkast Reddit Demo")
        }
        
        
        
    }
}





struct RedditPostListHeaderView: View {
    
    let post: RedditPostModel!
    
    var body: some View {
        HStack (spacing: 5) {
            Text("r/\(post.subreddit)")
                .foregroundColor(Color.orange)
                .font(.system(size: 14))
            
            
            Image(systemName: "circle.fill")
                .foregroundColor(Color.gray)
                .frame(width: 8.0, height: 8.0, alignment: .center)
                .cornerRadius(4)
                .clipped()
            
            Text("10h")
                .foregroundColor(Color.gray)
                .font(.system(size: 14))
            
            Image(systemName: "circle.fill")
                .foregroundColor(Color.gray)
                .frame(width: 8.0, height: 8.0, alignment: .center)
                .cornerRadius(4)
                .clipped()
            
            Text("r/\(post.author)")
                .foregroundColor(Color.gray)
                .font(.system(size: 14))
            
            Image(systemName: "circle.fill")
                .foregroundColor(Color.gray)
                .frame(width: CGFloat(8.0), height: CGFloat(8.0), alignment: .center)
                .cornerRadius(CGFloat(4))
                .clipped()
        }
    }
}


struct RedditPostListPostDescrriptionView: View {
    
    let post: RedditPostModel!
    
    var body: some View {
        HStack {
            
            StreamkastImageView(post: post)
            
            
            Text("\(post.title)")
                .foregroundColor(Color.black)
                .lineLimit(3)
        }
    }
}





struct RedditPostShareView: View {
    
    
    var body: some View {
        HStack {
            Image(systemName: "arrowshape.turn.up.right.fill")
                .foregroundColor(Color.gray)
            
            Text("Share")
                .foregroundColor(Color.gray)
                .font(.system(size: 14.0))
        }
    }
}








struct RedditPostLisLikeDisLikeViiew: View {
    
    let post: RedditPostModel!
    
    var body: some View {
        HStack(spacing: 5) {
            
            
            Image(systemName: "ellipsis")
                .foregroundColor(Color.gray)
            
            
            Divider()
                .frame(width:30)
            
            Image(systemName: "hand.thumbsup.fill")
                .foregroundColor(Color.gray)
            
            Text("\(post.ups.getThosendDevidedVale())")
                .foregroundColor(Color.gray)
                .font(.system(size: 14))
            
            Image(systemName: "hand.thumbsdown.fill")
                .foregroundColor(Color.gray)
            
        }

    }
}




struct RedditPostListCommentView: View {
    
    let post: RedditPostModel!
    
    var body: some View {
        HStack {
            Image(systemName: "ellipses.bubble.fill")
                .foregroundColor(Color.gray)
            
            Text("\(post.numComments.getThosendDevidedVale())")
                .foregroundColor(Color.gray)
                .font(.system(size: 14))
        }
    }
}



