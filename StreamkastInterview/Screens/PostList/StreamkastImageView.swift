//
//  StreamkastImageView.swift
//  StreamkastInterview
//
//  Created by Rahul Saini on 04/04/20.
//  Copyright © 2020 Streamkast. All rights reserved.
//

import SwiftUI
import URLImage

struct StreamkastImageView: View {

    let post: RedditPostModel
    
   var body: some View {
    URLImage(post.thumbURL!,
            placeholder: {
                ProgressView($0) { progress in
                    ZStack {
                        if progress > 0.0 {
                            CircleProgressView(progress).stroke(lineWidth: 8.0)
                        }
                        else {
                            CircleActivityView().stroke(lineWidth: 10.0)
                        }
                    }
                }
                    .frame(width: 10.0, height: 10.0)
            },
            content: {
                $0.image
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .clipShape(RoundedRectangle(cornerRadius: 5))
                    .shadow(radius: 10.0)
                    .frame(width: 100.0, height: 80.0)
                    .clipped()
            })
     
   }
}




