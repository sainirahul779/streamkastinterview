//
//  RedditPostCommentsView.swift
//  StreamkastInterview
//
//  Created by Rahul Saini on 04/04/20.
//  Copyright © 2020 Streamkast. All rights reserved.
//

import SwiftUI
import reddift



struct RedditPostCommentsView: UIViewControllerRepresentable {
    var post: RedditPostModel
    @Environment(\.presentationMode) var presentationMode
    
    func makeCoordinator() -> RedditPostCommentsView.Coordinator {
        return Coordinator(self)
    }
    
    func makeUIViewController(context: Context) -> CommentViewController {
        
        let controller = CommentViewController.initController(post: post)
        return controller
    }
    
    func updateUIViewController(_ uiViewController: CommentViewController, context: Context) {
        ReddiftSessionManager.shared.getComments(post: post)
    }
}

extension RedditPostCommentsView {
    class Coordinator: NSObject {
        var parent: RedditPostCommentsView
        @Environment(\.presentationMode) var presentationMode

        init(_ parent: RedditPostCommentsView) {
            self.parent = parent
        }

    }
}
