//
//  CommentViewController.swift
//  StreamkastInterview
//
//  Created by Rahul Saini on 05/04/20.
//  Copyright © 2020 Streamkast. All rights reserved.
//

import UIKit
import AlamofireImage
import reddift

class CommentViewController: UIViewController {

    @IBOutlet weak var thumbnilImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titlelbl: UILabel!
    var comments: [Comment] = []
    var post: RedditPostModel!
    var isSecondryCollapsed: [[Bool]] = []
    var isCommentCollapsed: [Bool] = []
    static func initController(post: RedditPostModel) -> CommentViewController{
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "CommentViewController") as? CommentViewController else {
            fatalError("")
        }
        controller.post = post
        return controller
    }
 

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(self.commentsFetched), name: Notification.Name(rawValue: "CommentsGetForPost"), object: nil)
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.titlelbl.text = post?.title
        if let url = post.thumbURL {
            self.thumbnilImageView.af_setImage(withURL: url)

        }
        
        self.tableView.separatorStyle = .none
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "CommentHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "CommentHeaderView")
        self.tableView.register(UINib(nibName: "CommentViewSecondryCell", bundle: nil), forCellReuseIdentifier: "CommentViewSecondryCell")
        
    }
    
    @objc func commentsFetched(notification: Notification) {
        print("=====================Noti")
        if let result = notification.userInfo as? [String: Any] {
            if let postObj = result["post"] as? RedditPostModel, postObj.id == self.post.id {
                if let comments = result["comments"] as? [Comment] {
                    self.comments = comments
                    self.isCommentCollapsed = Array(repeating: true, count: self.comments.count)
                    
                    for comment in comments {
                        self.isSecondryCollapsed.append(Array(repeating: true, count: comment.replies.children.count))
                    }
                    print(self.comments.count)
                    DispatchQueue.main.async {
                        self.tableView.reloadData()                        
                    }
                }
            }
        }
    }
}



extension CommentViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.comments.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isCommentCollapsed[section] {
            return 0
        }
        return self.comments[section].replies.children.count - 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CommentViewSecondryCell") as? CommentViewSecondryCell else {
            fatalError()
        }
        cell.delegate = self
        if let comment = self.comments[indexPath.section].replies.children[indexPath.row] as? Comment {
            cell.setupcell(comment: comment, isCollapsed: self.isSecondryCollapsed[indexPath.section][indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CommentHeaderView") as? CommentHeaderView else {
            fatalError("CommentHeaderView probably not registered")
        }
        headerView.setupHeader(comment: self.comments[section])
        headerView.delegate = self
        return headerView
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
}


extension CommentViewController: CommentHeaderViewDelegate {
    func tapOnComment(comment: Comment) {
        for i in 0..<self.comments.count where comments[i].id == comment.id {
            self.isCommentCollapsed[i] = !self.isCommentCollapsed[i]
            self.tableView.reloadData()
        }
    }
    
    
}


extension CommentViewController: CommentViewSecondryCellReloadData {
    func collapsed(isCollapsed: Bool, cell: UITableViewCell) {
        if let indexpath = self.tableView.indexPath(for: cell) {
            self.isSecondryCollapsed[indexpath.section][indexpath.row] = isCollapsed
            self.loadViewIfNeeded()
//            self.loadView()
            self.tableView.reloadData()
        }
    }
    
}
