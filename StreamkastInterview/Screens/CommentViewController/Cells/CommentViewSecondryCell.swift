//
//  CommentViewSecondryCell.swift
//  StreamkastInterview
//
//  Created by Rahul Saini on 05/04/20.
//  Copyright © 2020 Streamkast. All rights reserved.
//

import UIKit
import reddift

protocol CommentViewSecondryCellReloadData {
    func collapsed(isCollapsed: Bool, cell: UITableViewCell)
}
class CommentViewSecondryCell: UITableViewCell {
    @IBOutlet weak var tableView: UITableView!
    var comment: Comment!
    
    var isCollapsed = true
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tableView.register(UINib(nibName: "CommentViewSecondryHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "CommentViewSecondryHeaderView")
        self.tableView.register(UINib(nibName: "CommentViewThirdCell", bundle: nil), forCellReuseIdentifier: "CommentViewThirdCell")
    }
    var delegate: CommentViewSecondryCellReloadData?
    
    var numberOfRows: Int {
        if isCollapsed {
            return 0
        }
        return self.comment.replies.children.count - 1
    }
    
    func setupcell(comment: Comment, isCollapsed: Bool) {
        self.isCollapsed = isCollapsed
        print(comment.body)
        self.comment = comment
        self.tableView.separatorStyle = .none
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.reloadData()
        
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        tableView.layoutIfNeeded()
        
        tableView.frame = CGRect(x: 0, y: 0, width: targetSize.width, height: 1)
        let numberOfRows = self.numberOfRows + 1
        let height: CGFloat = CGFloat(numberOfRows * 80)
        
        let newSize = CGSize(width: self.frame.size.width, height: height)
        return newSize
    }
}


extension CommentViewSecondryCell: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Printing Comment\(comment.body)")
        print(numberOfRows)
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CommentViewThirdCell") as? CommentViewThirdCell else {
            fatalError()
        }
        print("Row\(indexPath.row)===============")
        if let comment = self.comment.replies.children[indexPath.row] as? Comment {
            print("Row\(indexPath.row)===============\(comment.body)")
            cell.setupHeader(comment: comment)
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CommentViewSecondryHeaderView") as? CommentViewSecondryHeaderView else {
               fatalError("CommentHeaderView probably not registered")
           }
           headerView.setupHeader(comment: self.comment)
           headerView.delegate = self
           return headerView
       }
       
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
    
}


extension CommentViewSecondryCell: CommentHeaderViewDelegate {
    func tapOnComment(comment: Comment) {
        
        isCollapsed = !isCollapsed
        self.delegate?.collapsed(isCollapsed: self.isCollapsed, cell: self)
    }
    
    
}
