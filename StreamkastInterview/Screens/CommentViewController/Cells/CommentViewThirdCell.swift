//
//  CommentViewSecondryCell.swift
//  StreamkastInterview
//
//  Created by Rahul Saini on 05/04/20.
//  Copyright © 2020 Streamkast. All rights reserved.
//

import UIKit
import reddift
class CommentViewThirdCell: UITableViewCell {

    
    @IBOutlet weak var commentView: UILabel!
      @IBOutlet weak var replyCount: UILabel!
      
     
     
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func setupHeader(comment: Comment) {
             print(comment.body)
             self.commentView.attributedText = stringFromHtml(string: comment.bodyHtml)
             self.replyCount.text = "Replies \(comment.replies.children.count)"
         }
 
    
}
