//
//  CommentViewSecondryHeaderView.swift
//  StreamkastInterview
//
//  Created by Rahul Saini on 05/04/20.
//  Copyright © 2020 Streamkast. All rights reserved.
//

import UIKit
import reddift

class CommentViewSecondryHeaderView: UITableViewHeaderFooterView {

  @IBOutlet weak var commentView: UILabel!
  @IBOutlet weak var replyCount: UILabel!
    
    
  var comment: Comment!
  var delegate: CommentHeaderViewDelegate?
    
  override func draw(_ rect: CGRect) {
      super.draw(rect)
      let gesture = UITapGestureRecognizer(target: self, action: #selector(self.didtap))
      self.commentView.addGestureRecognizer(gesture)
      
      let gesture1 = UITapGestureRecognizer(target: self, action: #selector(self.didtap))
      self.replyCount.addGestureRecognizer(gesture1)
  }
  
  func setupHeader(comment: Comment) {
      print(comment.body)
      self.commentView.attributedText = stringFromHtml(string: comment.bodyHtml)
      self.replyCount.text = "Replies \(comment.replies.children.count - 1)"
      self.comment = comment
  }
  
  @objc func didtap() {
      print("Tapped On Primary Comment")
      self.delegate?.tapOnComment(comment: self.comment)
  }
    

}
